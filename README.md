[![FINOS - Incubating](https://cdn.jsdelivr.net/gh/finos/contrib-toolbox@master/images/badge-incubating.svg)](https://github.com/finos/community/blob/master/governance/Software-Projects/Project-Lifecycle.md#incubating-projects)

# Structured Products
<img src="https://landscape.finos.org/logos/structured-products.svg"  width="100" >

## Background 
The FINOS Structured Products project, led by Goldman Sachs and FragmosChain, aims to accelerate the build and support of structured products in the ISDA Common Domain Model (CDM).

For background on the Structured Products project, see [github.com/finos/financial-objects/issues/66](url) and [github.com/finos/community/issues/133](url), as well as the FINOS Legend Pilot Case Study.

## Business Problem
The industry would like to assist and accelerate the build and support of structured products in the Common Domain Model.

**Problem Statement :**
·        How to standardize the representation of “Structured” Products
·        The CDM represents simple products across the five key derivative asset classes.
·        Industry adoption requires that CDM addresses products with more complex payouts.
 
**Approach** :

- Through a modeling working group, extend the CDM with the data features associated with structured products.

- Most of the building blocks are already available in the current state of the CDM. Expectation is preferably to leverage 
methodically existing items with some selective amendments or enhancements, rather than creating many new data types.

- The modeling effort will use the open-source freely accessible Legend Platform that already hosts the CDM.

- Output will be contributed iteratively to the CDM whenever consensus is reached.


## Development Team

The Project Steering Committee (“PSC”) will be responsible for all technical oversight of the Project. Initial members of the PSC will be set forth in the Project’s repository. The PSC may vote to change its composition at any time.

·        Ffion.Acland@gs.com or Vijayesh.Chandel@gs.com (Goldman Sachs) GitLab ID: ()

·        isloyan@isda.org (ISDA)

·        jbziade@fragmos-chain.com (Fragmos-Chain)

Target Contributors
- Derivative Product SMEs, Modelers, Data Engineers, CDM enthusiasts, Technologists


## Roadmap 

Project participants will begin by looking at the representation of the event function Knock-In and then progress to the representation of equity baskets. The proposal is to continue the work post completion of these two initial focus areas as the industry requires.

Modeling is being done in the FINOS hosted instance of Legend Studio, and a copy of the CDM in Legend Studio can be accessed at [text](https://legend.finos.org/studio/viewer/UAT-38.)

Please note that you will need to have an account on the FINOS hosted instance of Legend Studio in order to access it. You can request an account at [finos.org/legend](url).

Purpose :
Deliver proposals how to represent Structured Products in CDM

Methodological Deliverables x 5 :

1. Definition of a Structured Product, as compared to an Exotic Product

2. Framework “how to assess” whether to refactor or to create CDM objects, as compared to modelling with current CDM objects

3. Method “how to model” the features of Structured Products, notably the payout formula, thus resolving the question “how to represent algebric characters in CDM ?” – preliminary requirements for that purpose, before we decide :

4. Provide typology of the possible methods for clarification purposes (e.g. are CDM Product Objects sufficient and/or do we need CDM Functions to be involved and/or is there any other alternate approaches we should also contemplate ?)

5. Provide a ‘pros/cons’ analysis for each type of method

Scope of concrete Structured Products in terms of roadmap priorities e.g. Autocallable first (single/basket, cash/physicall) ?

- Method “how to challenge” our proposals in terms of Final Deliverables with regards to concrete Structured Products

Final Deliverables x “N” :
- Concrete list of “N” proposals (“N” number is not yet determined), each being a set of CDM objects to refactor or to create, in ready-for-prod format i.e. “Rosetta Workspace contributions”, submitted to the ARC for feedback before release in CDM

Modeling is being done in the FINOS hosted instance of Legend Studio, and a copy of the CDM in Legend Studio can be accessed at https://legend.finos.org/studio/viewer/UAT-38.

Please note that you will need to have an account on the FINOS hosted instance of Legend Studio in order to access it. You can request an account at finos.org/legend.


# Get Involved: Contribute to Structured Products

There are several ways to contribute to the Structured Products project:

**Join the next meeting**: 
Participants of the Structured Products project meet the first Tuesday at 9am ET / 2pm GMT. Find the next meeting on the FINOS Community calendar and browse past meeting minutes in GitHub. Email help@finos.org to be added to the recurring calendar invite.

**Join the mailing list**:
 Communications for the Structured Products project are conducted through the structured-products@lists.finos.org mailing list. Please email help@finos.org to be added to the mailing list.

**Propose changes to the model**:
Request an account on the FINOS Legend Studio hosted instance at finos.org/legend in order to access the model at https://legend.finos.org/studio/-/setup/UAT-28793838.

**Raise an issue**: 
If you have any questions or suggestions, please raise an issue

NOTE: Commits and pull requests to FINOS repositories will only be accepted from those contributors with an active, executed Individual Contributor License Agreement (ICLA) with FINOS OR who are covered under an existing and active Corporate Contribution License Agreement (CCLA) executed with FINOS. Commits from individuals not covered under an ICLA or CCLA will be flagged and blocked by the FINOS Clabot tool (or EasyCLA). Please note that some CCLAs require individuals/employees to be explicitly named on the CCLA.

Need an ICLA? Unsure if you are covered under an existing CCLA? Email help@finos.org


* **Join the mailing list**: Communications for the Structured Products standard are conducted through the structured-products@lists.finos.org. Please email [structured-products@lists.finos.org](mailto:structured-products@lists.finos.org) to join the mailing list.

* **Raise an issue**: if you have any questions or suggestions, please [raise an issue](https://gitlab.com/finosfoundation/legend/financial-objects/structured-products/-/issues?sort=created_date&state=opened)


## License

The technical charter that sets forth the responsibilities and procedures for technical contribution is here: [ISDA Structured Product Project Charter (002)](). This project uses the **ISDA CDM(TM) Version 2.0 Development License** ; you can read more in the [LICENSE](LICENSE) file.
